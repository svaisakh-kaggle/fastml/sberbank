import feather, subprocess

from fastai.imports import *
from fastai.structured import *

from sklearn.ensemble import RandomForestRegressor
from sklearn.externals import joblib
from pathlib import Path
from contextlib import contextmanager

shrun = lambda cmd: print(subprocess.getoutput(cmd))

def _make(self):
    self.mkdir(parents=True, exist_ok=True)
    return self
Path.make = _make

COMPETITION = 'sberbank-russian-housing-market'
DIR_DATA = (Path('~/.kaggle/Competitions').expanduser() / COMPETITION).make()
DIR_CHECKPOINTS = (Path.cwd() / 'Checkpoints').make()

@contextmanager
def workdir(path):
    path_cwd = os.getcwd()
    os.chdir(path)

    yield

    os.chdir(path_cwd)

def download_dataset(train=True):
    filename = 'train.csv' if train else 'test.csv'

    if (DIR_DATA / filename).exists(): return

    with workdir(DIR_DATA):
        shrun(f'kaggle competitions download -c {COMPETITION} -f {filename}')
        shrun(f'unzip {filename} {filename} && rm {filename}.zip')

def _show(self):
    max_columns = len(self.columns) if hasattr(self, 'columns') else 2
    with pd.option_context("display.max_rows", len(self),
                           "display.max_columns", max_columns):
            display(self)

pd.DataFrame.show = _show
pd.core.series.Series.show = _show

_rmse = lambda input_, target: ((input_ - target) ** 2).mean()

def _eval(self, x, y):
    return self.score(x, y), _rmse(self.predict(x), y)

RandomForestRegressor.eval = _eval

def split_val(*args, frac):
    assert np.std([len(a) for a in args]) == 0

    n_train = int(len(args[0]) * (1- frac))
    return [a[:n_train] for a in args], [a[n_train:] for a in args]

def _print_scores(model, data, metrics=False):
    (x, y), (x_val, y_val) = data

    r2, rmse = model.eval(x, y)
    print(f'Training - r^2: {r2:.4f}, RMSE: {rmse:.4f}')

    r2_val, rmse_val = model.eval(x_val, y_val)
    print(f'Validation - r^2: {r2_val:.4f}, RMSE: {rmse_val:.4f}')

    if hasattr(model, 'oob_score_'): print(f'oob: {model.oob_score_:.4f}')

    if metrics: return (r2, rmse), (r2_val, rmse_val)

def train_model(model, data, metrics=False):
    x, y = data[0]

    model.fit(x, y)
    metrics = _print_scores(model, data, metrics)

    return (model, metrics) if metrics else model

def submit(x, y, message, filename='Submission'):
    filename += '.csv'

    submission = pd.DataFrame({'price_doc': y}, index=x.id)

    with workdir(DIR_CHECKPOINTS):
        submission.to_csv(filename)

        shrun(f'gzip {filename}')
        shrun(f'kaggle competitions submit {COMPETITION} -f {filename}.gz -m "{message}"')
        shrun(f'rm {filename}.gz')

def get_nodes(tree, x):
    from collections import namedtuple
    TreeNode = namedtuple('TreeNode', 'q t e s v')
    nodes = [TreeNode(q, t, e, int(s), v)
         for q, t, e, s, v in zip(x.columns[tree.tree_.feature], tree.tree_.threshold,
                              tree.tree_.impurity, tree.tree_.weighted_n_node_samples,
                              tree.tree_.value.squeeze())]
    nodes = [nodes[i] for i in (0, 1, 8)]

    return nodes
